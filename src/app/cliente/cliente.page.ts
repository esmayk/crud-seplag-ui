import { DocumentoService } from './services/documento.service';
import { OverlayService } from './../core/services/overlay.service';
import { ClienteService } from './services/cliente.service';
import { Cliente } from './models/cliente.model';
import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { SalvaClienteComponent } from './components/salva-cliente/salva-cliente.component';

@Component({
  selector: 'app-home',
  templateUrl: 'cliente.page.html',
  styleUrls: ['cliente.page.scss'],
})
export class ClientePage implements OnInit {
  @Input() close: EventEmitter<any>;
  clientes$: Observable<Cliente[]>;

  constructor(
    private modalController: ModalController,
    private overlayService: OverlayService,
    private clienteService: ClienteService,
    private documentoService: DocumentoService
  ) {}

  async ngOnInit() {
    this.init();
  }

  async init() {
    const loading = await this.overlayService.loading();
    this.clientes$ = this.clienteService.findAll();
    this.clientes$.pipe(take(1)).subscribe(() => loading.dismiss());
  }

  async onNovoCliente() {
    const modal =  await this.modalController.create({
       backdropDismiss: true,
       component: SalvaClienteComponent,
       cssClass: 'action-bar-modal'
     });
    modal.onDidDismiss().then(() => this.init());
     await modal.present();
   }
 
   async onUpdate(cliente: Cliente) {
    const modal =  await this.modalController.create({
      backdropDismiss: true,
      component: SalvaClienteComponent,
      cssClass: 'action-bar-modal',
      componentProps: { cliente }
    });
    modal.onDidDismiss().then(() => this.init());
    await modal.present();
   }
 
   async onDelete(cliente: Cliente) {
    const loading = await this.overlayService.loading();
    this.clienteService.delete(cliente.id).subscribe(() => {
      this.clientes$ = this.clienteService.findAll();
      this.clientes$.pipe(take(1)).subscribe(() =>loading.dismiss());
    });
    await this.overlayService.toast({
      message: `O cliente de nome ${cliente.nome} removido com sucesso!`
    });
   }

   async onDownload(cliente: Cliente) {
    const loading = await this.overlayService.loading();
    this.documentoService.download(cliente.documentos[0].id).subscribe(
      pdf => {
        loading.dismiss();
        const fileURL = URL.createObjectURL(pdf.body);
        const link = document.createElement('a');
        link.href = fileURL;
        link.download = cliente.documentos[0].nome;
        link.click();
      },
      error => {
        loading.dismiss();
        this.overlayService.toast({ message: error.error ? error.error.message : error });
      }
    );
   }

}
