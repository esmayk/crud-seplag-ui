import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Cliente, ClienteDTO } from './../models/cliente.model';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  constructor(
    private http: HttpClient, 
  ) { }

  findById(id: number) {
    return this.http.get(`${environment.urlCrudSeplagAPI}/clientes/${id}`);
  }

  findAll():Observable<Cliente[]> {
    return this.http.get<Cliente[]>(`${environment.urlCrudSeplagAPI}/clientes`);
  }

  delete(id: number) {
    return this.http.delete(`${environment.urlCrudSeplagAPI}/clientes/${id}`);
  }

  insert(obj : ClienteDTO):Observable<Cliente>{
    return this.http.post<Cliente>(`${environment.urlCrudSeplagAPI}/clientes`, obj); 
  }

  update(obj : ClienteDTO) {
    return this.http.put(`${environment.urlCrudSeplagAPI}/clientes/${obj.id}`, obj);
  }
}
