import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DocumentoService {

  constructor(
    private http: HttpClient, 
  ) { }


  upload(arquivo: File, idCliente: number): Observable<any> {
    const formData = new FormData();
    formData.append('arquivo', arquivo, arquivo.name);
    return this.http.post<any>(`${environment.urlCrudSeplagAPI}/documentos/upload/${idCliente}`, formData);
  }

  download(id: number) {
    return this.http.get(`${environment.urlCrudSeplagAPI}/documentos/download/${id}`, { observe: 'response', responseType: 'blob' });
  }

}
