export interface Documento {
    id: number;
    nome: string;
    caminho: string;
    data: Date;
    arquivo: string;
}