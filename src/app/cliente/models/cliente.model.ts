import { Documento } from './documento.model';
export interface Cliente {
    id: number;
    nome: string;
    email: string;
    cpf: string;
    cnpj: string;
    tipo: string;
    dataCadastro: Date;
    documentos: Documento[];
}


export interface ClienteDTO {
    id?: number;
    nome: string;
    email: string;
    cpf: string;
    cnpj: string;
}