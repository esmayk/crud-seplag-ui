import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { ClientePage } from './cliente.page';

import { ClientePageRoutingModule } from './cliente-routing.module';

import { SalvaClienteComponent } from './components/salva-cliente/salva-cliente.component';
import { ItemClienteComponent } from './components/item-cliente/item-cliente.component';
import { UploadDocumentoComponent } from './components/upload-documento/upload-documento.component';


@NgModule({
  imports: [ SharedModule, ClientePageRoutingModule ],
  declarations: [ClientePage, ItemClienteComponent, SalvaClienteComponent, UploadDocumentoComponent ],
  entryComponents: [SalvaClienteComponent],
  exports: [ItemClienteComponent, SalvaClienteComponent, UploadDocumentoComponent]
})
export class ClientePageModule {}
