import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActionSheetController, AlertController } from '@ionic/angular';
import { Cliente } from '../../models/cliente.model';

@Component({
  selector: 'app-item-cliente',
  templateUrl: './item-cliente.component.html',
  styleUrls: ['./item-cliente.component.scss'],
})
export class ItemClienteComponent implements OnInit {
  @Input() cliente: Cliente;
  @Output() download = new EventEmitter<Cliente>();
  @Output() update = new EventEmitter<Cliente>();
  @Output() delete = new EventEmitter<Cliente>();


  constructor(
    private alertController: AlertController
  ) { }

  ngOnInit() {}

  async onDelete() {
    const alert = await this.alertController.create({
      header: 'Tem a certeza?',
      message: `Se deletar não voltará a ver este cliente ${this.cliente.nome}`,
      buttons: [
        {
          text: 'Nao',
          role: 'cancel',
        }, {
          text: 'Sim',
          handler: () => {
            this.delete.emit(this.cliente);
          }
        }
      ]
    });

    await alert.present();
  }


}
