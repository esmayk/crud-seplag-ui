import { ClienteService } from './../../services/cliente.service';
import { Cliente } from './../../models/cliente.model';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { OverlayService } from 'src/app/core/services/overlay.service';
import { DocumentoService } from '../../services/documento.service';
import { switchMap, catchError  } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-salva-cliente',
  templateUrl: './salva-cliente.component.html',
  styleUrls: ['./salva-cliente.component.scss'],
})
export class SalvaClienteComponent implements OnInit {
  @Input() cliente: Cliente;
  
  title: string
  buttonTitle: string;
  clienteForm: FormGroup;
  tipoCliente: string;
  arquivo: File;

  private cpfControl = new FormControl('', [Validators.required, Validators.minLength(11)]);
  private cnpjControl = new FormControl('', [Validators.required, Validators.minLength(17)]);


  constructor(
    private fb: FormBuilder,
    private modalController: ModalController,
    private overlayService: OverlayService,
    private clienteService: ClienteService,
    private documentoService: DocumentoService,
  ) { }

  ngOnInit() {
    this.createForm();
    this.init();
  }

  async onSubmit() {
    const loading = await this.overlayService.loading();
    if(this.cliente === undefined) {
      this.arquivo ? this.clienteService
        .insert(this.clienteForm.value)
          .pipe(
            switchMap((cliente) => {
              return this.documentoService.upload(this.arquivo, cliente.id);
            }),
            catchError((error) => {
              loading.dismiss();
              return throwError(error);
            })
          ).subscribe(() => {
            loading.dismiss();
            this.modalController.dismiss();
            this.overlayService.toast({ message: "Cliente salvo com sucesso!"});
          })
      : this.clienteService
          .insert(this.clienteForm.value)
          .subscribe(() => {
            loading.dismiss();
            this.modalController.dismiss();
            this.overlayService.toast({ message: "Cliente salvo com sucesso!"});  
          });
    } else {
      this.arquivo ? this.clienteService.update({ id: this.cliente.id, ...this.clienteForm.value })
          .pipe(
            switchMap(() => {
              return this.documentoService.upload(this.arquivo, this.cliente.id);
            }),
            catchError((error) => {
              loading.dismiss();
              return throwError(error);
            })
          ).subscribe(() => {
            loading.dismiss();
            this.modalController.dismiss();
            this.overlayService.toast({ message: "Cliente alterado com sucesso!"});
          })
          : this.clienteService.update({ id: this.cliente.id, ...this.clienteForm.value
          }).subscribe(()=> {
          loading.dismiss();
          this.modalController.dismiss();
          this.overlayService.toast({ message: "Cliente alterado com sucesso!"});
        });   
    }
    
  }

  upload(event) {
    this.arquivo = event.arquivo;
  }

  fishyHandler(event) {
    this.tipoCliente = event.target.value;
    if (this.tipoCliente !== 'PF') {
      this.clienteForm.removeControl('cpf');
      this.clienteForm.addControl('cnpj', this.cnpjControl);
    } else {
      this.clienteForm.removeControl('cnpj');
      this.clienteForm.addControl('cpf', this.cpfControl);
    }
  }

  leaveFish(event) {
    this.tipoCliente = event.target.value;
  }
  goFish(event) {
    this.tipoCliente = event.target.value;
  }

  private createForm(): void {
    this.clienteForm = this.fb.group({
      nome: ['', [Validators.required, Validators.minLength(6)]],
      email: ['', [Validators.required, Validators.email]],
    });
  }

  init() {
    if(this.cliente === undefined) {
      this.title =  "Novo cliente";
      this.buttonTitle = "salvar";
      this.tipoCliente = "PF";
      this.clienteForm.addControl('cpf', this.cpfControl);
    } else {
      this.title = "Editar cliente";
      this.buttonTitle = "alterar";
      this.clienteForm.get('nome').setValue(this.cliente.nome);
      this.clienteForm.get('email').setValue(this.cliente.email);
      if(this.cliente.cpf !== undefined && this.cliente.cpf !== '' && this.cliente.cpf !== null) {  
        this.tipoCliente = "PF";
        this.clienteForm.addControl('cpf', this.cpfControl);
        this.clienteForm.get('cpf').setValue(this.cliente.cpf);
      } else {
        this.tipoCliente = "PJ";
        this.clienteForm.addControl('cnpj', this.cnpjControl);
        this.clienteForm.get('cnpj').setValue(this.cliente.cnpj);
      }
    }
  }

  get name(): FormControl {
    return <FormControl>this.clienteForm.get('name');
  }

  get email(): FormControl {
    return <FormControl>this.clienteForm.get('email');
  }

}
