import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { interval } from 'rxjs';
import { OverlayService } from 'src/app/core/services/overlay.service';


@Component({
  selector: 'app-upload-documento',
  templateUrl: './upload-documento.component.html',
  styleUrls: ['./upload-documento.component.scss'],
})
export class UploadDocumentoComponent implements OnInit {
  @Output() upload = new EventEmitter();
  @ViewChild('fileInput') fileInput: ElementRef;

  arquivo: File;
  showProgress = false;
  timeouProgress: number;

  constructor(
    private overlayService: OverlayService,
    private alertController: AlertController
  ) { }

  ngOnInit() {}


  fileChange(event) {
    const fileList: FileList = event.target.files;
    this.arquivo = fileList[0];
    if (this.arquivo) {
      if (this.arquivo.type !== 'application/pdf') {
        this.fileInput.nativeElement.value = '';
        this.arquivo = undefined;
        this.overlayService.toast({ message: 'Permitido apenas arquivos em pdf'});
      } else {
       this.showProgressBar();
       const name = this.arquivo.name;
       this.arquivo = new File([this.arquivo], name, { type: this.arquivo.type });
       this.upload.emit({ arquivo: this.arquivo });
      }
    }
  }

  async visualizaPdf() {
    let fileURL: string;
    let blob: Blob;
    const loading = await this.overlayService.loading();
    if ( this.arquivo ) {
      blob = new Blob([ this.arquivo ], { type: 'application/pdf' });
      fileURL = URL.createObjectURL(blob);
      window.open(fileURL);
      loading.dismiss();
    }
  }

  nomeDoArquivo(nomeArquivo: string): string {
    const filenameThreshold = 20;
    let arquivo = '';
    if (nomeArquivo) {
      arquivo = nomeArquivo;
    }
    const half = filenameThreshold / 2;
    if (arquivo.length > filenameThreshold) {
      const start = arquivo.substring(0, half);
      const end = arquivo.substring(arquivo.length, arquivo.length - half);
      return start + '...' + end;
    }
    return arquivo;
  }

  async confirmeRemoveFile() {
    const alert = await this.alertController.create({
      header: 'Tem a certeza?',
      message: `Se deletar não voltará a ver este arquivo ${this.arquivo.name}`,
      buttons: [
        {
          text: 'Nao',
          role: 'cancel',
        }, {
          text: 'Sim',
          handler: () => {
            this.removeFile();
          }
        }
      ]
    });
    await alert.present();
   }

  removeFile() {
    if (this.arquivo) {
      this.fileInput.nativeElement.value = '';
      this.arquivo = undefined;
    }
  }

  showProgressBar() {
    const source = interval(600);
    let intervalSub$: any;
    const count = 0.2;
    this.showProgress = true;
    this.timeouProgress =  0;
    intervalSub$ = source.subscribe((index) => {
      this.timeouProgress += count;
      if ( index >= 5 ) {
        intervalSub$.unsubscribe();
        this.showProgress = false;
      }
    });
  }

  titleize(text) {
    const words = text.toLowerCase().split(' ');
    for (let a = 0; a < words.length; a++) {
      const w = words[a];
      words[a] = w[0].toUpperCase() + w.slice(1);
    }
    return words.join(' ');
  }

}
